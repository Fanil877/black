$(document).ready(function() {

    $('#checkbox').on('change', function() {
        if ($('#checkbox').prop('checked')) {
            $('#submit').attr('disabled', false);
        } else {
            $('#submit').attr('disabled', true);
        }
    });

    let inputsArrId = ['emailInput']
    let inputs = {
        email: false
    }

    $('#emailInput').on('change keyup paste', function() {
        $(this).removeClass('error')

        if ($(this).val().length > 0 && inputs.email === false) {
            inputs.email = true
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)
                }
            }
        }
        if ($(this).val().length === 0 && inputs.email === true) {
            inputs.email = false
            inputsArrId.push($(this).attr('id'))
        }
    })


    $('#submit').on('click', function(e) {
        for (let i = 0; i <= inputsArrId.length; i++) {
            $('#' + inputsArrId[i] + '').addClass('error')
        }
        if (inputsArrId.length == 0) {
            $('#feedback form').addClass('no-active');
            $('#feedback .success').removeClass('no-active');
            $('#emailInput').val('')
            inputsArrId = ['emailInput']
            inputs = {
                email: false
            }
        }
    })

    document.addEventListener('invalid', (function() {
        return function(e) {
            e.preventDefault();
        };
    })(), true);
})